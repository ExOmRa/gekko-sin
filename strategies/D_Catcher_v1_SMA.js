/*
	DreamCatcher v1.0 - Buying bears spikes
		The_Foundation - Сравниваем текущую цену с SMA.

		Modules
		v1	MultiPairs - ability to trading many pairs.
		v1	VolumeSpotter - corrects incoming data for proper correlation with exchanges and services.
		v2	ATR_AutoLeveler - for more accurate buy\sell. Modificating buy\sell set points when current ATR exceed average ATR value.
		v2	ATR_Saver - blocking trade if current volatility exceed specified level. Calculated as ratio middle ATR to current.


	~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
	Author - TeoWay
	License - Use or modification only with the author permission
	Site - http://TeoWay.com
	Repository - https://gitlab.com/TeoWay/
	~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
*/
var log = require('../core/log');

// Create strat
var strat = {};

// Prepare everything
strat.init = function(){
	this.requiredHistory = this.tradingAdvisor.historySize;
	
	// Options
	this.Digits = 8;
	this.Debug = false; // Debug? If false - improves performance in backtests (use console.log or log.info)
	this.ModulesStopDebug = false; // Show when modules stoping trade
	this.MultiPairs = false; // MultiPairs
	this.VolumeSpotter = 0; // 0-off 1-tradeview 2-binance
	
	// Base Vars
	this.TradeNumber = 1;
	this.CandleCounter = 0;
	this.PreviousAction = 0;

	// Preparing logic and components 
		// The_Foundation
		this.setSMA_Interval = this.settings.SMA_Interval;
		this.setBuyValue = this.settings.BuyDifPercent;
		this.setSellValue = this.settings.SellDifPercent;
		switch(this.VolumeSpotter){
			case 0:
				this.addIndicator('SMA', 'SMA', this.settings.SMA_Interval);
				break;
			case 1:
				this.addIndicator('SMA', 'SMA-tradeview', this.settings.SMA_Interval);
				break;
			case 2:
				this.addIndicator('SMA', 'SMA-binance', this.settings.SMA_Interval);
				break;				
		}
		
		// MultiPairs
		(this.MultiPairs) ? this.fs = require('fs'):''; 
		
		// VolumeSpotter
		this.VolSpo_CandleCounter = 0;
		this.VolSpo_WarmupAddingCandles = 0;
		
		// ATR_AutoLeveler
		switch(this.VolumeSpotter){
			case 0:
				this.addIndicator('atr_ATR_AL', 'ATR', this.settings.ATR_AutoLeveler.ATR_Interval);
				break;
			case 1:
				this.addIndicator('atr_ATR_AL', 'ATR-tradeview', this.settings.ATR_AutoLeveler.ATR_Interval);
				break;
			case 2:
				this.addIndicator('atr_ATR_AL', 'ATR-binance', this.settings.ATR_AutoLeveler.ATR_Interval);
				break;				
		}

		this.setATR_AL_Enabled = this.settings.ATR_AutoLeveler.Enabled;
		this.setATR_AL_Interval = this.settings.ATR_AutoLeveler.Module_Interval; 
		this.setATR_AL_OnBuySensitivity = this.settings.ATR_AutoLeveler.OnBuySensitivity;
		this.setATR_AL_OnSellSensitivity = this.settings.ATR_AutoLeveler.OnSellSensitivity;
		
		this.ATR_AL_Sum = 0;
		this.ATR_AL_Mid = 0;
		this.ATR_AL_Values = [];
		
		
		
		// ATR_Saver
		switch(this.VolumeSpotter){
			case 0:
				this.addIndicator('atr_ATR_Sa', 'ATR', this.settings.ATR_Saver.ATR_Interval);
				break;
			case 1:
				this.addIndicator('atr_ATR_Sa', 'ATR-tradeview', this.settings.ATR_Saver.ATR_Interval);
				break;
			case 2:
				this.addIndicator('atr_ATR_Sa', 'ATR-binance', this.settings.ATR_Saver.ATR_Interval);
				break;				
		}		
			
		this.setATR_Sa_Enabled = this.settings.ATR_Saver.Enabled;
		this.setATR_Sa_Interval = this.settings.ATR_Saver.Module_Interval; 
		this.setATR_Sa_MaxPercent = this.settings.ATR_Saver.MaxPercent;
		
		this.ATR_Sa_Sum = 0;
		this.ATR_Sa_Mid = 0;
		this.ATR_Sa_Values = [];
}

// Warmup and every new candle
strat.update = function(candle){
	// VolumeSpotter
	if ((this.VolumeSpotter) && (candle.volume == 0)){
		this.VolSpo_CandleCounter++;
		return;
	}
	
	// Base
	this.CandleCounter++;
	this.ActualPrice = candle.close;
	this.ModulesBuyStop = [];
	this.ModulesSellStop = [];

	// The_Foundation
	if (this.requiredHistory <= this.setSMA_Interval){
		console.log('The_Foundation Warmup Error, Min Warmup Time - ')+console.log(this.setSMA_Interval + 1);
		process.exit(1);
 	}
	this.indicators.SMA.update(candle.close);
	if (this.CandleCounter >= this.setSMA_Interval){
		this.SMA = this.indicators.SMA.result;
		this.ActualBuyDifPercent = ((this.ActualPrice/this.SMA * 100) - 100); // Считаем соотношение цен текущей свечи и SMA
	}
	
	// Module - ATR_AutoLeveler
	if ((this.setATR_AL_Enabled) && (this.CandleCounter >= this.settings.ATR_AutoLeveler.ATR_Interval)){
		if (this.requiredHistory <= (this.settings.ATR_AutoLeveler.ATR_Interval + this.setATR_AL_Interval)){
			console.log('ATR_AutoLeveler Warmup Error, Min Warmup Time - ')+console.log(this.settings.ATR_AutoLeveler.ATR_Interval + this.setATR_AL_Interval + 1);
			process.exit(1);
		}
		this.ATR_AL_Values.push(this.indicators.atr_ATR_AL.result);
		for (var i = 0; i < this.ATR_AL_Values.length; i++ ){
			this.ATR_AL_Sum += parseFloat(this.ATR_AL_Values[i]); // parseFloat ибо ошибка Node JS при додавании элементов, начинает считать их строкой
		}
		
		if (this.ATR_AL_Values.length >= this.setATR_AL_Interval){
			this.ATR_AL_Mid = (this.ATR_AL_Sum / this.ATR_AL_Values.length);
			
			// Влияем на значения уровней покупок и продаж 
			this.setBuyValue = this.settings.BuyDifPercent; //CfS
			this.setSellValue = this.settings.SellDifPercent; //CfS

			this.ATR_AL_ChgBuyValue = this.indicators.atr_ATR_AL.result / this.ATR_AL_Mid * this.setATR_AL_OnBuySensitivity;
			this.ATR_AL_ChgSellValue = this.indicators.atr_ATR_AL.result / this.ATR_AL_Mid * this.setATR_AL_OnSellSensitivity;
			
			this.setBuyValue = this.setBuyValue - this.ATR_AL_ChgBuyValue + this.setATR_AL_OnBuySensitivity;
			this.setSellValue = this.setSellValue + this.ATR_AL_ChgSellValue - this.setATR_AL_OnSellSensitivity;

			this.ATR_AL_Sum = 0;
			this.ATR_AL_Values.shift();
		}
	}

	// Module - ATR_Saver
	if ((this.setATR_Sa_Enabled) && (this.CandleCounter >= this.settings.ATR_Saver.ATR_Interval)){
		if (this.requiredHistory <= (this.settings.ATR_Saver.ATR_Interval + this.setATR_Sa_Interval)){
			console.log('ATR_Saver Warmup Error, Min Warmup Time - ')+console.log(this.settings.ATR_Saver.ATR_Interval + this.setATR_Sa_Interval + 1);
			process.exit(1);
		}		
		this.ATR_Sa_Values.push(this.indicators.atr_ATR_Sa.result);
		for (var i = 0; i < this.ATR_Sa_Values.length; i++ ){
			this.ATR_Sa_Sum += parseFloat(this.ATR_Sa_Values[i]);
		}
		
		if (this.ATR_Sa_Values.length >= this.setATR_Sa_Interval){
			this.ATR_Sa_Mid = (this.ATR_Sa_Sum / this.ATR_Sa_Values.length);
			
			this.ATR_Sa_CurPercent = (this.indicators.atr_ATR_Sa.result / this.ATR_Sa_Mid * 100) - 100;
			
			this.ATR_Sa_Sum = 0;
			this.ATR_Sa_Values.shift();	
			
			if (this.ATR_Sa_CurPercent > this.setATR_Sa_MaxPercent){
				this.ModulesBuyStop.push('- ATR_Saver');
				(this.settings.ATR_Saver.StopBuyOnly) ?'':this.ModulesSellStop.push('- ATR_Saver');
			}
		}
	}
}

// War
strat.check = function(candle){
	// VolumeSpotter
		if ((this.VolumeSpotter) && (!(this.VolSpo_WarmupDone))){
			(this.VolSpo_Trigger1)?'':console.log('Waiting additional not less that '+this.VolSpo_CandleCounter+' candles (VolumeSpotter)');
			if(this.VolSpo_WarmupAddingCandles !== this.VolSpo_CandleCounter){
				this.VolSpo_Trigger1 = 1;
				this.VolSpo_WarmupAddingCandles++;
				return;
			} else{
				this.VolSpo_WarmupDone = true;
				console.log('VolumeSpotter warmup correction Done! Added '+this.VolSpo_WarmupAddingCandles+' candles');
			}
		}
	// Long
	if ((this.PreviousAction < 2) && (this.ActualBuyDifPercent <= this.setBuyValue)){
		// MultiPairs
		if ((this.MultiPairs) && (this.fs.existsSync('!!!-inTrade.txt'))){
			console.log('Skip! We in trade');
			return;
		} else if (this.MultiPairs){
			this.fs.writeFileSync("!!!-inTrade.txt");
		}

		// Modules Stops
		if (this.ModulesBuyStop.length > 0){
			if (this.ModulesStopDebug === true){
				console.log('Modules Buy STOP:');
				for (var i = 0; i < this.ModulesBuyStop.length; i++ ){
					console.log(this.ModulesBuyStop[i]);
				}
			}
			return;
		}
		
		// Execution	
		this.advice('long');
		this.BuyPrice = this.ActualPrice;
		this.PreviousAction = 2;
		
		// Debug
		if (this.Debug === true){
			console.log('~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~');
			console.log('Trade №', this.TradeNumber++);
			console.log(candle.start)
			console.log('- Buy Price:', this.ActualPrice.toFixed(this.Digits));
			console.log('- Buy DifPerc:', this.ActualBuyDifPercent.toFixed(this.Digits));
			console.log('- Buy SMA:', this.SMA.toFixed(this.Digits));
			
			// Module - ATR_AutoLeveler
			if (this.setATR_AL_Enabled){
				console.log('- - - ATR AutoLeveler - - -');
				console.log('- Cur/Mid/Perc:', this.indicators.atr_ATR_AL.result.toFixed(this.Digits) ,'/',this.ATR_AL_Mid.toFixed(this.Digits),'/',this.ATR_AL_ChgBuyValue.toFixed(this.Digits));
				console.log('- Min Buy DifPerc:', this.setBuyValue.toFixed(this.Digits));
			}
			
			// Module - ATR_Saver
			if (this.setATR_Sa_Enabled){
				console.log('- - - ATR Saver - - -');
				console.log('- Cur/Mid/Perc:', this.indicators.atr_ATR_Sa.result.toFixed(this.Digits),'/',this.ATR_Sa_Mid.toFixed(this.Digits),'/',this.ATR_Sa_CurPercent.toFixed(this.Digits));
			}

			console.log('');
		}
	}
	
	// Short
	this.actualSellDifPercent = (this.ActualPrice/this.BuyPrice * 100) - 100;
	if ((this.PreviousAction == 2) && (this.actualSellDifPercent >= this.setSellValue)){
		// MultiPairs
		(this.MultiPairs) ? this.fs.unlinkSync("!!!-inTrade.txt"):'';

		// Modules Stops
		if (this.ModulesSellStop.length > 0){
			if (this.ModulesStopDebug === true){
				console.log('Modules Sell STOP:');
				for (var i = 0; i < this.ModulesSellStop.length; i++ ){
					console.log(this.ModulesSellStop[i]);
				}
			}
			return;
		}
		
		// Execution
		this.advice('short');
		this.PreviousAction = 1;
		
		// Debug
		if (this.Debug === true){
			console.log(candle.start)
			console.log('- Sell Price:', this.ActualPrice.toFixed(this.Digits));
			console.log('- Sell DifPerc:', this.actualSellDifPercent.toFixed(this.Digits));
			
			// Module - ATR_AutoLeveler
			if (this.setATR_AL_Enabled){
				console.log('- - - ATR AutoLeveler - - -');
				console.log('- Cur/Mid/Perc:', this.indicators.atr_ATR_AL.result.toFixed(this.Digits) ,'/',this.ATR_AL_Mid.toFixed(this.Digits),'/',this.ATR_AL_ChgSellValue.toFixed(this.Digits));
				console.log('- Min Sell DifPerc:', this.setSellValue.toFixed(this.Digits));
			}
			
			// Module - ATR_Saver
			if (this.setATR_Sa_Enabled){
				console.log('- - - ATR Saver - - -');
				console.log('- Cur/Mid/Perc:', this.indicators.atr_ATR_Sa.result.toFixed(this.Digits),'/',this.ATR_Sa_Mid.toFixed(this.Digits),'/',this.ATR_Sa_CurPercent.toFixed(this.Digits));
			}			
		}
	}
}

module.exports = strat;
